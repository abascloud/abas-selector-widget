= Dokumentation abas Selector-Widget
:page-layout: docs
:toc: left
:doctype: book
:icons: font
:copyright: abas Software GmbH

== Beschreibung & Funktionen
//Description of the widget and its features

Mit dem Selector-Widget können Sie Einträge aus einer Datenbank auswählen und das Dashboard mit seinen Widgets in einen Kontext stellen. Der Eintrag im Selector-Widget kann gesetzt werden, indem Sie einen Wert in der Dropdown-Liste auswählen oder angeben, oder indem er über den URL-Parameter "context" voreingestellt wird (z. B. …​&context=(154,0:1,0), weitere Informationen siehe Dokumentation Dashboard). Über den Button "X" können Sie einen ausgewählten Eintrag entfernen.

Sonderfall Auswahlfeld "Mitarbeiter": In den Einstellungen können Sie festlegen, ob das Selector-Widget mit dem entsprechenden abas ERP-Mitarbeiter Ihres angemeldeten Benutzers vorbelegt werden soll. Aktivieren oder deaktivieren Sie hierzu das Feld "Angemeldeten Benutzer automatisch vorauswählen". 

== Anwendungsfälle
//List of use cases the widget can be used for
- Auswählen eines Kunden, um die entsprechenden Daten in einem anderen Widget anzuzeigen.
- Auswählen eines Mitarbeiters, um seine Aufgaben in einem Listings-Widget anzuzeigen.

== Lernvideo
video::https://documentation.abas.cloud/en/dashboard/videos/Tutorial_7.webm[width=640]

== Beispiele
//Images of examples what the widget could look like

.Auswahlfeld für Lieferanten
image::selector_widget_vendor.jpg[Auswahlfeld für Lieferanten]

.Auswahlfeld für Mitarbeiter
image::selector_widget_employee.jpg[Auswahlfeld für Mitarbeiter]

.Dropdown-Liste Auswahlfeld
image::selector_widget_selection.jpg[Dropdown-Liste Auswahlfeld]

.Auswahlfeld ohne Beschriftung
image::selector_widget_without_label.jpg[Auswahlfeld ohne Beschriftung]

.Leeres Auswahlfeld
image::selector_widget_empty.jpg[Leeres Auswahlfeld]

== Einstellungen
//Description of the settings dialog (e.g. necessary settings, optional settings, etc.)
.Einstellungsdialog
image::selector_widget_settings.jpg[Einstellungsdialog]


=== abas ERP-Mandant
Sie können für die in diesem Widget verarbeiteten und angezeigten Daten einen Standardkontext festlegen, indem Sie einen abas ERP-Mandanten auswählen. Dieser Kontext kann über den URL-Parameter "mandant" überschrieben werden, sofern er nicht als "Fixiert" markiert ist.

=== Datenbank
Wählen Sie hier die abas ERP-Datenbank aus, aus der Sie Ihre Datensätze auswählen möchten. Alle Datenbanken aus abas ERP stehen auch hier zur Verfügung. 

=== Titel
Tragen Sie hier den Titel ein, den Sie im Widget anzeigen möchten.

==== Widget-Gruppe
Alle Widgets innerhalb derselben Widget-Gruppe kommunizieren miteinander. Die Standard-Widget-Gruppe ist "default". Sie können die Widget-Gruppe, zu der ein Widget gehört, ändern, um ein Widget abzugrenzen oder um Widgets zu gruppieren.


