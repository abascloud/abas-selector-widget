# 1.0.4

- Added the possibility to reset the selection

# 1.0.3

- adapted the user interface to new design

# 1.0.2

- Fixed a bug where selecting records wasn't possible anymore due to an update of a used component

# 1.0.1

- The message bus context coming in via url now works not only with record id (erp field "id") but also with ident number (erp field "nummer")

# 1.0.0

- New Widget! This new selector widget allows to select a record and distribute it via message bus.
If a context is given via url ("...&context=0:1;(226,0,0)"), it is preselected upon loading.
